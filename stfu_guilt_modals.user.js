// ==UserScript==
// @name         STFU guilt modals
// @namespace    https://social.coop/gdorn
// @version      0.1
// @description  Automatically click 'STFU' links in modals, particularly about ads.
// @author       @gdorn@social.coop
// @match        https://www.rockpapershotgun.com/*
// @match        https://medium.com/*
// @grant        none
// ==/UserScript==

var start = function() {
    var url = window.location.href;
    if (url.includes('rockpapershotgun.com')){
        setTimeout(function(){click_dismiss_text('button', 'Continue ad blocking');}, 500);
    } else if (url.includes('medium.com')){
        console.log("Closing medium.com overlay...");
        click_xpath('//button[contains(@data-action, "overlay-close")]');
    }
}

function get_element_with_text(tag_name, text){
    var elements = document.getElementsByTagName(tag_name);
    for (var i = 0; i < elements.length; i++){
        if (elements[i].textContent == text){
            return elements[i];
        }
    }
}


var click_dismiss_text = function(tag_name, text) {
// find a button with this text and click it.
    var element = get_element_with_text(tag_name, text);
    if (!element){
        setTimeout(function(){click_dismiss_text(tag_name, text);}, 500);
    } else {
    element.click();
    }
}

var click_xpath = function(xpath){
    console.log("Looking for xpath:", xpath);
    var matchingElement = document.evaluate(xpath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
    if (!matchingElement){
        console.log("Nothing matching xpath, waiting:", xpath);
        setTimeout(function(){click_xpath(xpath);}, 500);
    }
    console.log("Clicking xpath:", xpath, matchingElement);
    matchingElement.click();
}

setTimeout(start, 500);


/*
var load,execute,loadAndExecute;load=function(a,b,c){var d;d=document.createElement("script"),d.setAttribute("src",a),b!=null&&d.addEventListener("load",b),c!=null&&d.addEventListener("error",c),document.body.appendChild(d);return d},execute=function(a){var b,c;typeof a=="function"?b="("+a+")();":b=a,c=document.createElement("script"),c.textContent=b,document.body.appendChild(c);return c},loadAndExecute=function(a,b){return load(a,function(){return execute(b)})};
loadAndExecute("//code.jquery.com/jquery-3.3.1.slim.min.js", function() {
    console.log("Starting...");
    start();
});
*/
